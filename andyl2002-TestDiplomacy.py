#!/usr/bin/env python3

# Andy Liu, Franklin Tang

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):

    def test_solve_1(self):
        # failure case for army name invalid
        r = StringIO("ABDFG Madrid Move London\n")
        w = StringIO()
        self.assertRaises(ValueError, diplomacy_solve, *(r, w))

    def test_solve_2(self):
        # failure case for multiple moves, trips assertion in diplomacy_read()
        r = StringIO("A London Hold\nB Seoul Support A Hold\nC Barcelona Move Support Hold C\n")
        w = StringIO()
        self.assertRaises(AssertionError, diplomacy_solve, *(r, w))

    def test_solve_3(self):
        # failure case for invalid move
        r = StringIO("A Madrid conquer London\nB London Hold\n")
        w = StringIO()
        self.assertRaises(ValueError, diplomacy_solve, *(r, w))

    def test_solve_4(self):
        # test only one army, moving to a non-starting city
        r = StringIO("A Madrid Move London\n")
        w = StringIO()
        self.assertRaises(AssertionError, diplomacy_solve, *(r, w))

    def test_solve_5(self):
        # test two armies supporting each other
        r = StringIO("A Madrid Support B\nB London Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\n")

    def test_solve_6(self):
        # If an army is attacked while supporting another army, its support becomes invalid - even when supporting the attacker
        r = StringIO("A Madrid Move London\nB London Support A\nC Seoul Support A\nD Barcelona Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Seoul\nD Barcelona\n")

    def test_solve_7(self):
        # If an army moves, it does not come into conflict with an army attacking its original city
        r = StringIO("A Madrid Move London\nB London Move Madrid\nC Seoul Support A\nD Barcelona Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Madrid\nC Seoul\nD Barcelona\n")

    def test_solve_8(self):
        # Losing and gaining support
        r = StringIO("A Madrid Move London\nB London Move Seoul\nC Seoul Support A\nD Austin Move London\nE Newark Support D\nF Lisbon Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Seoul\nD London\nE Newark\nF Lisbon\n")
    
    def test_solve_9(self):
        # single winner in battle with winner attacking
        r = StringIO("A Madrid Move London\nB London Hold\nC Seoul Support A\nD Barcelona Support A\nE Lisbon Move London\nF NYC Support E\nG LA Support E\nH Houston Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Seoul\nD Barcelona\nE London\nF NYC\nG LA\nH Houston\n")

    def test_solve_10(self):
        # ensure raise from assertion when army name is repeated
        r = StringIO("A Madrid Hold\nA London Hold")
        w = StringIO()
        self.assertRaises(AssertionError, diplomacy_solve, *(r, w))
    
    def test_solve_11(self):
        # ensure raise from assertion when two armies start in same city
        r = StringIO("A Madrid Hold\nB Madrid Hold")
        w = StringIO()
        self.assertRaises(AssertionError, diplomacy_solve, *(r, w))

    def test_solve_12(self):
        # ensure raise from assertion when an army moves to a city that is not previously occupied
        r = StringIO("A Madrid Hold\nB London Move Seoul")
        w = StringIO()
        self.assertRaises(AssertionError, diplomacy_solve, *(r, w))

if __name__ == '__main__':
    main() #pragma: no cover
