#!/usr/bin/env python3


# Diplomacy Test File

# Imports
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_check_clash, diplomacy_print, diplomacy_solve

# test class

class TestDiplomacy (TestCase):
    # read function
    def test_read_1(self):
        s = "A Madrid Hold\n"
        read_output = diplomacy_read(s)
        self.assertEqual(read_output, ['A Madrid Hold'])
    
    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        read_output = diplomacy_read(s)
        self.assertEqual(read_output, ['B Barcelona Move Madrid'])
    
    def test_read_3(self):
        s = "D Austin Move London\n"
        read_output = diplomacy_read(s)
        self.assertEqual(read_output, ['D Austin Move London'])

    # solve
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----
if __name__ == "__main__": main()